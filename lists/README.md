# lists

These are the lists that have been developed throughout the whole project.

The first list ever made, ``list-Prates.csv``, is based on Prates's list. It has all the professions they used with the number of appearances in the training corpus used for this study.

``list-250-professions.txt``is the second list, which has 250 common professions from the training corpus. 

The third list,``sample-profession-list-n``, is based on the second, as we filtered it. When we had approximately 100 common professions, we looked for different translations in Spanish, both in dictionaries and the corpus itself. This is the reason why this list has 350 lines despite only being made by 100 professions. In addition, each line has its six regular expressions: English singular, English plural, Spanish feminine singular, Spanish feminine plural, Spanish masculine singular, Spanish masculine plural. 
 
``list-filter.csv``, the fourth file, shows how the list has been reduced based on the presence in the training corpus of each of the lines. Therefore, the different columns with numbers on top (1000, 500, 300 and 100) indicate whether the profession appears at least that number of times or not. If the column says "no", that means that the number of appearances is not smaller than the number of the column. Moreover, the columnn called "diferencia" ("difference") shows the difference between the appearances of the Spanish word for women and men.

Finally, ``final-list.txt`` is the output of the filtering and the list used for the study.


