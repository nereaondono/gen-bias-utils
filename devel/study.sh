# use template.m4
# to generate a temporary shell script
# and execute it
#
# Mikel L. Forcada 2000 under the GPL v3 license


shell_m4=$(mktemp)".m4" 
echo "define(ENGWORD,"$1")dnl"  >$shell_m4
echo "define(SPAWORD,"$2")dnl" >>$shell_m4
echo "define(NUMBER,"$3")dnl"  >>$shell_m4
cat  template.m4               >>$shell_m4
shell=$(mktemp)
m4 $shell_m4 >$shell
rm $shell_m4
chmod +x $shell
bash $shell $4 $5
rm $shell
