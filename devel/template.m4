# This template needs to be processed into a shell by replacing:
# ENGWORD with a regular expression for an English root, such as [Nn]urse
# SPAWORd with a regular expression for a Spanish root, such as [Ee]nfermer
# NUMBER with a number of examples to be studied
# (this can easily be done with m4:
# define(ENGWORD,[Nn]urse)dnl
# define(SPAWORD,[Ee]nfermer)dnl
# define(NUMBER,1000)dnl
#
# and it will indeed be done by calling m4 via another shell.
#
# it reads as standard input a parallel corpus (Moses style), with English and Spanish
# using TAB as separator.
# © Mikel L Forcada 2020 under the GPL v3 license
# 
# )
locale=C # This is because we use bc for calculations
file1=$(mktemp)".base" # temporary file
# select
# - translation units containing at least one appearance of the English word on the English side
# - but no more than one
# - containing at least one appearance of the Spanish word on the Spanish side
# - and no more than NUMBER lines in translation units
paste $1 $2 | grep "ENGWORD[s]\?[^[:alpha:]].*[\t]" | grep -v "ENGWORD[s]\?[^[:alpha:]].*[^[:alpha:]]ENGWORD[s]\?[^[:alpha:]].*[\t]" | grep "[\t].*SPAWORD[ao]s\?[^[:alpha:]]" | head -NUMBER  >$file1
# count how many such lines we got
nTot=$(cat $file1 | wc -l)

# and report
printf "Of a sample of nTot=%d\n" $nTot

# Count how many times this corresponds to a single masculine word in Spanish 
nMasc=$(cat $file1 | grep "[\t].*SPAWORD[o]s\?[^[:alpha:]]" | grep -v "[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]].*[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]]"| wc -l)

# Count how many times this corresponds to a single feminine word in Spanish
nFem=$(cat $file1 | grep "[\t].*SPAWORD[a]s\?[^[:alpha:]]" | grep -v "[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]].*[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]]"| wc -l )

# Number of times appearing as a doublet masc and/or fem (no triplet)
nMascFem=$(cat $file1 | grep "[\t].*SPAWORD[o]s\?[^[:alpha:]].*[eyou,].*[^[:alpha:]]SPAWORD[a]s\?[^[:alpha:]]" | grep -v "[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]].*[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]].*[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]]" | wc -l );

# Number of times appearing as a doublet fem and/or masc (no triplet)
nFemMasc=$(cat $file1 | grep "[\t].*SPAWORD[a]s\?[^[:alpha:]].*[eyou,].*[^[:alpha:]]SPAWORD[o]s\?[^[:alpha:]]" | grep -v "[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]].*[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]].*[^[:alpha:]]SPAWORD[ao]s\?[^[:alpha:]]" | wc -l );
# report findings
printf "nMasc=%d (%s%%)\n" $nMasc $(echo 100*$nMasc/$nTot | bc -l)
printf "nFem=%d (%s%%)\n" $nFem $(echo 100*$nFem/$nTot | bc -l)
printf "nMascFem=%d (%s%%)\n" $nMascFem $(echo 100*$nMascFem/$nTot | bc -l)
printf "nFemMasc=%d (%s%%)\n" $nFemMasc $(echo 100*$nFemMasc/$nTot | bc -l)
printf "Other=%d (%s%%)\n" $[nTot-nMasc-nFem-nMascFem-nFemMasc] $(echo "("$nTot"-"$nMasc"-"$nFem"-"$nMascFem"-"$nFemMasc")/"$nTot"*100" |bc -l) 

# delete temporary file
rm $file1
