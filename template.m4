#
# This template needs to be processed into a shell by replacing:
# ENGSG with a regular expression for a singular English noun, such as [Nn]urse
# ENGPL with a regular expression for a plural English noun, such as [Nn]urses
# SPAMSG with a regular expression for a masculine singular Spanish noun such as [Ee]nfermero
# SPAMPL with a regular expression for a masculine plural Spanish noun such as [Ee]nfermeros
# SPAFSG with a regular expression for a feminine singular Spanish noun such as [Ee]nfermera
# SPAFPL with a regular expression for a feminine plural Spanish noun such as [Ee]nfermeras
# NUMBER with the base number of examples
# (this can easily be done with m4:
# define(ENGSG,[Nn]urse)dnl
# define(ENGPL,[Nn]urses)dnl
# define(SPAMSG,[Ee]nfermero)dnl
# define(SPAMPL,[Ee]nfermeros)dnl
# define(SPAFSG,[Ee]nfermera)dnl
# define(SPAFPL,[Ee]nfermeras)dnl
# define(NUMBER,1000)dnl
#
# and it will indeed be done by calling m4 via another shell.
#
# it reads as standard input a parallel corpus (Moses style), with English and Spanish
# using TAB as separator.
# © Mikel L Forcada 2020 under the GPL v3 license
# Some factoring / preprocessing of regular expressions could occur here
# or in the m4 processor to make this clearerT
# 
# )
locale=C # This is because we use bc for calculations

# Name temporary files
basename=$(mktemp) # temporary base name
total=$basename".all" # all files
masc=$basename".masc"
fem=$basename".fem"
mascfem=$basename".mascfem"
femmasc=$basename".femmasc"



# select
# - translation units containing at least one appearance of the English word on the English side
# - but no more than one
# - containing at least one appearance of the Spanish word on the Spanish side
# - and no more than NUMBER lines in translation units
paste $1 $2 | grep "\(^\|[^[:alpha:]]\)\(ENGSG\|ENGPL\)[^[:alpha:]].*[\t]" | grep -v "\(^\|[^[:alpha:]]\)\(ENGSG\|ENGPL\)[^[:alpha:]].*[^[:alpha:]]\(ENGSG\|ENGPL\)[^[:alpha:]].*[\t]" | grep "[\t]\(.*[^[:alpha:]]\)\?\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" | head -NUMBER  >$total

# count how many such lines we got (should be NUMBER)
nTot=$(cat $total | wc -l)

# and report
# printf "Of a sample of nTot=%d\n" $nTot

# Count how many times this corresponds to a single masculine word in Spanish 

cat $total | grep "[\t].*[^[:alpha:]]\(SPAMSG\|SPAMPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$masc
nMasc=$(cat $masc | wc -l)


# Count how many times this corresponds to a single feminine word in Spanish
cat $total | grep "[\t].*[^[:alpha:]]\(SPAFSG\|SPAFPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$fem

nFem=$(cat $fem | wc -l)
# Number of times appearing as a doublet masc and/or fem (no triplet)
cat $total | grep "[\t].*[^[:alpha:]]\(SPAMSG\|SPAMPL\)\([^[:alpha:]]\+[eyou][^[:alpha:]]\+\|,[^[:alpha:]]\+\)\(SPAFSG\|SPAFPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$mascfem
nMascFem=$(cat $mascfem | wc -l)
# Number of times appearing as a doublet fem and/or masc (no triplet)
cat $total | grep "[\t].*[^[:alpha:]]\(SPAFSG\|SPAFPL\)\([^[:alpha:]]\+[eyou][^[:alpha:]]\+\|,[^[:alpha:]]\+\)\(SPAMSG\|SPAMPL\)[^[:alpha:]]" | grep -v "[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]].*[^[:alpha:]]\(SPAMSG\|SPAFSG\|SPAMPL\|SPAFPL\)[^[:alpha:]]" >$femmasc
nFemMasc=$(cat $femmasc | wc -l)

printf \""%15s\", \"%15s\", \"%15s\", \"%15s\", \"%15s\", \"%15s\", %6d, %6d, %6d, %6d, %6d \n" "ENGSG" "ENGPL" "SPAFSG" "SPAFPL" "SPAMSG" "SPAMPL" $nFem $nMasc $nFemMasc $nMascFem $[nTot-nMasc-nFem-nMascFem-nFemMasc] 

# report findings
# printf "nMasc=%d (%s%%)\n" $nMasc $(echo 100*$nMasc/$nTot | bc -l)
# printf "nFem=%d (%s%%)\n" $nFem $(echo 100*$nFem/$nTot | bc -l)
# printf "nMascFem=%d (%s%%)\n" $nMascFem $(echo 100*$nMascFem/$nTot | bc -l)
# printf "nFemMasc=%d (%s%%)\n" $nFemMasc $(echo 100*$nFemMasc/$nTot | bc -l)
# printf "Other=%d (%s%%)\n" $[nTot-nMasc-nFem-nMascFem-nFemMasc] $(echo "("$nTot"-"$nMasc"-"$nFem"-"$nMascFem"-"$nFemMasc")/"$nTot"*100" |bc -l) 

# delete temporary file if "delete" was passed

if [ "$3" == "delete" ]
then
rm $total
rm $masc
rm $fem
rm $femmasc
rm $mascfem
fi
