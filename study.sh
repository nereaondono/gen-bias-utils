# use template.m4
# to generate a temporary shell script
# and execute it
#
# Mikel L. Forcada 2020 under the GPL v3 license

shell_m4=$(mktemp)".m4" 
echo "define(ENGSG,"$1")dnl"  >$shell_m4
echo "define(ENGPL,"$2")dnl" >>$shell_m4
echo "define(SPAFSG,"$3")dnl"  >>$shell_m4
echo "define(SPAFPL,"$4")dnl"  >>$shell_m4
echo "define(SPAMSG,"$5")dnl"  >>$shell_m4
echo "define(SPAMPL,"$6")dnl"  >>$shell_m4
echo "define(NUMBER,"$7")dnl"  >>$shell_m4
cat  template.m4               >>$shell_m4
shell=$(mktemp)".sh"
m4 $shell_m4 >$shell
rm $shell_m4
chmod +x $shell
bash $shell $8 $9 ${10} # source, target and optional "delete" to delete intermediate files
rm $shell
